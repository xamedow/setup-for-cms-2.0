<!doctype html>
<html lang="ru" ng-app="wizard">
<head ng-controller="HeadCtrl as head">
    <meta charset="UTF-8">
    <title>Мастер настройки проекта "{{ steps[0].content[0].value || 'Новый проект' }}"</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/app/css/style.css"/>
</head>
<body ng-controller="MainCtrl as main" ng-cloak>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation" ng-include="'/app/partials/navbar.html'"></div>
<div class="container theme-showcase" role="main" ng-cloak>
    <div class="alert alert-danger" ng-show="data.error">
        <strong>Ошибка #{{ data.error.code }}</strong>
        <a href="#" ng-click="newPage()">Попробуйте перезагрузить страницу</a>
    </div>
    <div class="row" ng-hide="data.error">
        <div class="col-sm-8">
            <div class="panel panel-default" ng-show="checkTab(step.name)" ng-repeat="step in data.steps">
                <div class="panel-heading">{{ step.title }}</div>
                <div class="panel-body">
                    <form action="">
                        <div class="form-group" ng-repeat="field in step.content"
                             ng-include="getTemplateFromType(field.type)">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-danger pull-right" ng-click="nextTab($index)"
                                    ng-show="$index < data.steps.length - 1">
                                Далее
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-sm-4" ng-include="'/app/partials/sidebar.html'"></div>
    </div>
</div>
<div ng-view></div>
<script src="/app/js/angular.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular-route.js"></script>
<script src="/app/js/controllers/cms_setup.js"></script>
</body>
</html>