<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Неверные параметры"</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/app/css/style.css"/>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Создание нового проекта</a>
        </div>
    </div>
</div>
<div class="container theme-showcase" role="main" ng-cloak>
    <div class="alert alert-danger">
        <strong>Ошибка:</strong><span> неверные параметры страницы<span>
        <a href="http://crm.cherepkova.ru">Сначала выберите проект в ЦРМ</a>
    </div>
</div>
</body>
</html>