(function () {
    var app = angular.module('wizard', ['ngRoute']),

        steps = [
            {
                name: 'meta',
                title: 'Мета информация проекта',
                done: false,
                visible: true,
                content: [
                    {
                        name: 'project_name',
                        placeholder: 'Название проекта',
                        value: '',
                        required: true
                    },
                    {
                        name: 'project_domain',
                        placeholder: 'Название домена',
                        value: ''
                    },
                    {
                        name: 'main_email',
                        placeholder: 'Основной email',
                        value: ''
                    }
                ]
            },
            {
                name: 'type',
                title: 'Тип проекта',
                done: false,
                visible: false,
                content: [
                    {
                        name: 'project_type',
                        placeholder: 'Тип проекта',
                        type: 'select',
                        value: '',
                        required: true
                    },
                    {
                        name: 'project_type_variants',
                        placeholder: 'Вариант типа',
                        type: 'select',
                        value: ''
                    }
                ]
            },
            {
                name: 'modules',
                title: 'Основные модули',
                done: false,
                visible: false,
                content: [
                    {
                        name: 'modules',
                        placeholder: 'Основной модуль',
                        type: 'multi-select',
                        value: ''
                    }
                ]
            },
            {
                name: 'mockup',
                title: 'Загрузка макета',
                done: false,
                visible: false,
                content: [
                    {
                        name: 'project_type',
                        placeholder: 'Загрузить файлы макета',
                        type: 'mockup-upload',
                        value: ''
                    }
                ]
            },
            {
                name: 'secondary_modules',
                title: 'Вторичные модули',
                done: false,
                visible: false,
                content: [
                    {
                        name: 'project_type',
                        placeholder: 'Тип проекта',
                        value: ''
                    }
                ]
            },
            {
                name: 'finish',
                title: 'Готово!',
                done: false,
                visible: false,
                content: [{
                    type: 'final'
                }]
            }
        ];
    app.filter('presetModules', function () {
        return function (modules, modulesList) {
            modules = modules || '';
            var out = {},
                i = 0, l = modules.length;
            for (; i < l; ++i) {
                if (modules[i].id in modulesList) {
                    out[i] = modules[i];
                }
            }
            return out;
        }
    });
    app.filter('freeModules', function () {
        return function (modules, modulesList) {
            var out = [],
                i = 0, l = modules.length;
            for (; i < l; ++i) {
                if (!(modules[i].id in modulesList)) {
                    out.push(modules[i]);
                }
            }
            return out;
        };
    });
    app.controller('HeadCtrl', function ($scope) {
        $scope.steps = steps;

    });
    app.controller('MainCtrl', function ($scope, $window, $http) {
        $scope.data = {};
        $scope.data.steps = steps;
        $scope.tab = 'meta';
        $scope.progress = 0;
        $scope.modulesList = {};
        var host = $window.location.href;

        function setData(subject) {
            $http.get(host + 'get/' + subject).success(function (data) {
                angular.forEach($scope.data.steps, function (step) {
                    if (step.content) {
                        angular.forEach(step.content, function (item) {
                            if (item.name === subject) {
                                item.data = data;
                            }
                        });
                    }
                });
            }).error(function (error) {
                $scope.data.error = error;
            });
        }

        setData('project_type');
        setData('project_type_variants');
        setData('modules');
        /**
         * Проверка активной вкладки.
         * @param tab
         * @returns {boolean}
         */
        $scope.checkTab = function (tab) {
            return $scope.tab === tab;
        };

        $scope.newPage = function () {
            $window.location.reload();
        };
        /**
         * Аксессор для свойства tab.
         * @param tab
         */
        $scope.setTab = function (tab) {
            $scope.tab = tab;
        };

        /**
         * Check if fields, that have required attribute is filled up.
         * Проверяет заполнены ли поля с аттрибутом required.
         * @param step
         * @returns {boolean}
         */
        $scope.stepDone = function (step) {
            var content = step.content,
                i = 0,
                l = content.length;
            for (; i < l; ++i) {
                if (content[i].required && !content[i].value) {
                    return false;
                }
            }
            return true;
        };

        /**
         * Переходит на следующую вкладку, проводя валидацию текущей формы и изменяя визуальный статус.
         * @param i
         */
        $scope.nextTab = function (i) {
            if ($scope.stepDone($scope.data.steps[i])) {
                var next = $scope.data.steps[i + 1];
                $scope.tab = next.name;
                next.visible = true;
                if ($scope.progress < 100) {
                    $scope.changeProgress(i);
                }
            }
        };

        /**
         * Меняет статус прогресс бара в меню.
         * @param i
         */
        $scope.changeProgress = function (i) {
            var segment = 100 / $scope.data.steps.length;
            $scope.progress = Math.floor((i + 1) * segment + segment);
        };

        /**
         * Возвращает имя класса, при завeршении всех шагов.
         * @returns {string}
         */
        $scope.isDoneClass = function () {
            return $scope.progress === 100 ? 'panel-success' : 'panel-default';
        };
        $scope.getTemplateFromType = function (type) {
            return type ? '/app/partials/' + type + '.html' : '/app/partials/input.html';
        };

        function replaceModulesList(item) {
            $scope.modulesList = {};
            if (item && angular.isString(item)) {
                var itemR = item.split(','),
                    i = 0, l = itemR.length;
                for (; i < l; ++i) {
                    $scope.modulesList[itemR[i]] = true;
                }
            }
        }

        function getNextId(data) {
            if(angular.isArray(data)) {
                var i = 0, l = data.length, max = 0;
                for(; i < l; ++i) {
                    if ( angular.isObject(data[i]) && ('id' in data[i]) && data[i].id > max) {
                        max = data[i].id;
                    }
                }
                return max + 1;
            }
        }

        function addToModulesList(item, fieldData) {
            if (angular.isString(item) && angular.isArray(fieldData)) {
                var id = getNextId(fieldData);
                fieldData.push({id: id, name: item});
                $scope.modulesList[id] = true;
            } else if (item.name.toLowerCase() !== 'свой вариант') {
                $scope.modulesList[item.id] = true;
            }
        }

        $scope.addToList = function (item, fieldData) {
            if (!angular.isObject(item) || !('modules' in item)) {
                addToModulesList(item, fieldData);
            } else if (item && 'modules' in item) {
                replaceModulesList(item.modules);
            }
        };

        $scope.deleteFromList = function (item) {
            delete $scope.modulesList[item.id];
        }
    });
}());