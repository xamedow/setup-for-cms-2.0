<?php
$token = 'andrew';
if (isset($_POST['token']) && $token === $_POST['token']) {
	$module = $_POST['module'];
    $file = $module.'.7z';

    if (file_exists($file)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        ob_clean();
        flush();
        readfile($file);
        exit;
    }
} else {
	header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
}
# output from db
