<?php
    namespace main;

    class FileHandler {

        /**
         *  Записывает указанный текст в указанный файл.
         *
         * @param $file
         * @param $text
         */
        public static function writeTo($file, $text)
        {
            if (!file_exists($file)) {
                touch($file);
            }

            file_put_contents($file, $text, FILE_APPEND | LOCK_EX);
        }


        /**
         * Читает файл, возвращает строку.
         *
         * @param $file_path
         *
         * @return string
         * @throws \Exception
         */
        public static function readToString($file_path)
        {
            if (file_exists($file_path)) {
                return file_get_contents($file_path);
            } else {
                throw new \Exception('File not found: ' . $file_path);
            }
        }

        /** Читает файл, возвращает массив строк.
         *
         * @param $file_path
         *
         * @return array
         * @throws \Exception
         */
        public static function readToArray($file_path)
        {
            if (file_exists($file_path)) {
                return file($file_path);
            } else {
                throw new \Exception('File not found: ' . $file_path);
            }
        }

        /**
         * Возвращает массив подпапок первого уровня вложенности.
         *
         * @param $folder_path
         *
         * @return array
         * @throws \Exception
         */
        public static function foldersToArray($folder_path)
        {
            $out = array();
            if ($handle = opendir($folder_path)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry !== '.' && $entry !== '..') {
                        $out[] = $entry;
                    }
                }
                closedir($handle);

                return $out;
            } else {
                throw new \Exception('Folder not found: ' . $folder_path);
            }
        }

        /**.
         * Возвращает массив файлов первого уровня вложенности из указанной папки с указанным расширением.
         *
         * @param $folder_path
         * @param $ext
         *
         * @return array
         * @throws \Exception
         */
        public static function filesToArray($folder_path, $ext)
        {
            $out = array();
            if ($handle = opendir($folder_path)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry !== '.' && $entry !== '..' && pathinfo(DROOT . "/$folder_path/$entry", PATHINFO_EXTENSION) === $ext) {
                        $out[] = $entry;
                    }
                }
                closedir($handle);

                return $out;
            } else {
                throw new \Exception('Folder not found: ' . $folder_path);
            }
        }

        /**
         * Проверяет аргумент(строка пути) на существование папок,
         * Если папка не обнаружена, флаг resolve указывает, создавать папку, либо выкидывать ошибку.
         *
         * @param $path string
         * @param $resolve bool
         *
         * @return bool
         */
        public static function resolvePath($path, $resolve = true)
        {
            // Убираем из пути папки до корня сайта (C:\domain\docs\).
            $chunks = explode('/', substr((string)$path, strlen(DROOT) + 1));

            // Проверяем каждый элемент пути на существование.
            $out = DROOT;
            $pattern = '/\.\w{2,4}$/i';
            for ($i = 0; $i < count($chunks); $i++) {
                $out .= '/' . $chunks[ $i ];
                if ($out !== DROOT && preg_match($pattern, $out) === 0 && !file_exists($out)) {
                    if ($resolve) {
                        mkdir($out);
                    } else {
                        return false;
                    }
                }
            }

            return $out;
        }
    }