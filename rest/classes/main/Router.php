<?php
    namespace main;

    class Router {

        private static $urlParts = array(
            'action',
            'subject'
        );

        public function __construct()
        {
            self::setUrlParts();
            self::callHandler();
        }

        private static function setUrlParts()
        {
            $parts_arr = array_filter(explode('/', RURI));
            $parts_arr = count($parts_arr) < count(self::$urlParts)
                ? array_pad($parts_arr, count(self::$urlParts), null)
                : $parts_arr;
            self::$urlParts = array_combine(self::$urlParts, $parts_arr);
        }

        /**
         * Выбирает шаблон, исходя из содержимого массива _POST
         */
        private static function chooseTemplate() {
            $fpost_object = new Filter($_POST, 'string');
            $fpost = $fpost_object->apply();
            if(
                isset($fpost['action']) &&
                $fpost['action'] === 'contstruct' &&
                isset($fpost['user_id']) &&
                isset($fpost['project_id'])
            ) {
                include_once DROOT . '/views/setup_wizard.php';
            } else {
                include_once DROOT . '/views/no_auth.php';
                FileHandler::writeTo('logs/construct_errors.log', 'Пустые данные от ip: ' . $_SERVER['REMOTE_ADDR'] . PHP_EOL);
            }
        }

        private static function callHandler()
        {
            $handler = ucfirst(self::$urlParts['action']) . 'Handler';
            if (file_exists(DROOT . '/rest/classes/main/' . $handler . '.php')) {
                $handler = '\\main\\' . $handler;
                $inst = new $handler(self::$urlParts['subject']);
                $inst->init();
            } else {
                self::chooseTemplate();
            }
        }
    }