<?php
    /**
     * Date: 19.09.2014
     * Time: 22:15
     */

    namespace main;


    class GetHandler extends RestHandler implements HandlerInterface {

        private $data = null;

        public function init()
        {
            $this->setData();
            header('Content-Type: application/json');
            echo json_encode($this->data);
        }

        private function setData()
        {
            $this->data = Db::queryExec("SELECT * FROM st_list_{$this->subject}");
        }
    }