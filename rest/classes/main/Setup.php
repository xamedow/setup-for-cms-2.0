<?php

    namespace main;

    use main\File_handler as fh;

    class Setup {

        /**
         * Константа для адреса репозитория модулей.
         */
        const MODSERVER = 'http://codetail.ru/modules.php';
        private $options = array();
        private $modules = array();

        public function __construct()
        {
            self::setOptions();
        }

        /**
         * Получает ассоциативный массив из файла options.json
         */
        private function setOptions()
        {
            $this->options = json_decode(fh::read_to_string('options.json'), true);
        }

        /**
         * Скачивает указанный модуль из репозитория в папку /temp/modules/module.7z.
         *
         * @param $module
         *
         * @return string
         */
        function getModule($module)
        {
            $file = $this->getFileName($module);
            fh::resolvePath($file);
            $token = (string)$this->options['token']; // TODO: check this.
            $data = array('token' => $token, 'module' => $module);
            fh::download($file, self::MODSERVER, $data);
        }

        /**
         * Получает имя фала для записи архива модуля.
         *
         * @param $module
         *
         * @return string
         */
        private function getFileName($module)
        {
            return $_SERVER['DOCUMENT_ROOT'] . '/temp/modules/' . $module . '.7z';
        }

    }