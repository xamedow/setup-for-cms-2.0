<?php
    /**
     * Date: 31.08.14
     * Time: 10:38
     */

    namespace main;


    class Translate {

        public static $enru = array(
            'phone'   => 'Телефон',
            'address' => 'Адрес',
            'city'    => 'Город',
            'email'   => 'Электронная почта',
            'name'    => 'Имя',
            'months'  => array(
                ''          => '',
                'December'  => 'Декабря',
                'January'   => 'Января',
                'February'  => 'Февраля',
                'March'     => 'Марта',
                'April'     => 'Апреля',
                'May'       => 'Мая',
                'June'      => 'Июня',
                'July'      => 'Июля',
                'August'    => 'Августа',
                'September' => 'Сентября',
                'October'   => 'Октября',
                'November'  => 'Ноября',
            )
        );
    }