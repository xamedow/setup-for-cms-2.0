<?php
    /**
     * Date: 19.09.2014
     * Time: 22:23
     */

    namespace main;


    interface HandlerInterface {

        public function __construct($subject);

        public function init();
    }