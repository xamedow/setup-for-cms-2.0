<?php
    namespace main;

    use main\FileHandler as fh;

    /**
     * Singleton класс авторизации пользователей.
     * Class Auth
     * @package main
     */
    class Auth {

        private static $instance;
        private        $auth = false;
        private        $person_id;

        /**
         * При создании экземпляра класса, проверяет логин/пароль, если есть данные от формы и юзер не авторизован.
         * Проверяет авторизацию, либо ставя флаг, либо записывая данные в сессию.
         * Выходит из сеанса при несовпадении хешей из сессии и базы.
         */
        private function __construct()
        {
            $this->setId();
            $this->checkHash();
            $this->checkAuth();
            $this->banUser();
        }

        /**
         * Проверяет
         */
        private function setId()
        {
            // TODO make $_POST filter.
            if (!$this->auth && isset($_POST['auth_form'])) {
                $pass = substr(sha1(trim($_POST['pass'])), 4, -4);
                $login = trim($_POST['login']);
                $id = current(Db::queryExec("SELECT id FROM mod_users WHERE pass = :pass AND login = :login", array(
                    ':pass'  => $pass,
                    ':login' => $login
                )));
                $this->person_id = $id['id'];
                $this->setHash();
                $this->setSessionId();
            }
        }

        /**
         * Устанавливает случайный хеш для текущего пользователя, записывая его в бд и в сессию.
         */
        private function setHash()
        {
            $hash = md5(rand(1, 5000));
            $_SESSION['hash'] = $hash;

            Db::queryExec("UPDATE mod_users SET session_hash = :hash WHERE id = :id", array(
                ':hash' => $hash,
                ':id'   => $this->person_id
            ), false);
        }

        /**
         *
         */
        private function setSessionId()
        {
            $_SESSION['user_id'] = !empty($this->person_id) ? $this->person_id : '';
        }

        /**
         * Сверяет хеш в сессии с хешем в бд, снимает авторизацию, если они не равны.т
         */
        private function checkHash()
        {
            if (!empty($_SESSION['user_id']) && !empty($_SESSION['hash'])) {
                $db_hash = current(Db::queryExec("SELECT session_hash FROM mod_users WHERE id = :id", array(':id' => $_SESSION['user_id'])));
                $db_hash = $db_hash['session_hash'];

                if ($db_hash !== $_SESSION['hash']) {
                    $_SESSION['login_error'] = 'Другой пользователь зашел под данной учетной записью.';
                    $_SESSION['user_id'] = '';
                    $this->auth = false;
                }
            }
        }

        /**
         * Устанавливает значение авторизационного флага либо логирует ошибку входа.
         * @throws \Exception
         */
        private function checkAuth()
        {
            if (!empty($_SESSION['user_id'])) {
                $this->auth = true;
            } else {
                if (isset($_POST['auth_form'])) {
                    $this->logFailure();
                }
            }
        }

        /**
         * Логирует неверную авторизацию. Записывает ссобщение об ошибке в сессию.
         */
        private function logFailure()
        {
            $_SESSION['login_error'] = 'Введенные вами данные неверны.';
            $ip = $_SERVER['REMOTE_ADDR'];
            $date = new \DateTime();
            $this->setFailCookie();
            fh::writeTo('logs/loginFailure', "$ip:{$date->getTimestamp()}\n");
        }

        /**
         *
         */
        private function setFailCookie()
        {
            for ($i = 1; $i < 10; ++$i) {
                if (!isset($_COOKIE["loginfailed_$i"])) {
                    break;
                }
            }

            $date = new \DateTime();
            setcookie("loginfailed_$i", $date->getTimestamp(), time() + 3600);
        }

        private function banUser()
        {
            if (file_exists('logs/loginFailure')) {
                $failures = fh::readToArray('logs/loginFailure');
                if (!empty($failures)) {
                    //            $failures = Misc::get_param($failures);
                }
            }
        }

        /**
         * Возвращает экземпляр данного класса.
         * @return Auth
         */
        public static function getInstance()
        {
            if (empty(self::$instance)) {
                self::$instance = new Auth;
            }

            return self::$instance;
        }

        /**
         * Возвращает значение auth.
         * @return bool
         */
        public function getAuth()
        {
            return $this->auth;
        }

        private function countFailCookies()
        {
            $count = 0;
            if (!empty($_COOKIE)) {
                foreach ($_COOKIE as $name => $cookie) {
                    if (strpos($name, 'loginfailed') !== false) {
                        ++$count;
                    }
                }
            }

            return $count;
        }
    }