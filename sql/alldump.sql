-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.17-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              8.3.0.4820
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица cms_setup.st_list_modules
CREATE TABLE IF NOT EXISTS `st_list_modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Дамп данных таблицы cms_setup.st_list_modules: ~8 rows (приблизительно)
/*!40000 ALTER TABLE `st_list_modules` DISABLE KEYS */;
INSERT INTO `st_list_modules` (`id`, `name`) VALUES
	(1, 'Свой вариант'),
	(2, 'Контент'),
	(3, 'Услуги'),
	(4, 'Каталог'),
	(5, 'Лендинг'),
	(7, 'Админка'),
	(8, 'Корзина'),
	(9, 'Личный кабинет');
/*!40000 ALTER TABLE `st_list_modules` ENABLE KEYS */;


-- Дамп структуры для таблица cms_setup.st_list_parents
CREATE TABLE IF NOT EXISTS `st_list_parents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `child_tb` varchar(255) NOT NULL DEFAULT '0',
  `parent_tb` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы cms_setup.st_list_parents: ~1 rows (приблизительно)
/*!40000 ALTER TABLE `st_list_parents` DISABLE KEYS */;
INSERT INTO `st_list_parents` (`id`, `child_tb`, `parent_tb`) VALUES
	(1, 'st_list_modules', 'st_list_project_type'),
	(8, 'st_list_modules', 'st_list_project_type_variants');
/*!40000 ALTER TABLE `st_list_parents` ENABLE KEYS */;


-- Дамп структуры для таблица cms_setup.st_list_project_type
CREATE TABLE IF NOT EXISTS `st_list_project_type` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `modules` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы cms_setup.st_list_project_type: ~6 rows (приблизительно)
/*!40000 ALTER TABLE `st_list_project_type` DISABLE KEYS */;
INSERT INTO `st_list_project_type` (`id`, `name`, `modules`) VALUES
	(1, 'Свой вариант', NULL),
	(2, 'Визитка', '2,7'),
	(3, 'Услуги', '2,3,7'),
	(4, 'Лендинг', '5,7'),
	(5, 'Каталог', '2,4,7'),
	(6, 'Магазин', '2,4,7,8');
/*!40000 ALTER TABLE `st_list_project_type` ENABLE KEYS */;


-- Дамп структуры для таблица cms_setup.st_list_project_type_variants
CREATE TABLE IF NOT EXISTS `st_list_project_type_variants` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- Дамп данных таблицы cms_setup.st_list_project_type_variants: ~3 rows (приблизительно)
/*!40000 ALTER TABLE `st_list_project_type_variants` DISABLE KEYS */;
INSERT INTO `st_list_project_type_variants` (`id`, `name`) VALUES
	(1, 'Свой вариант'),
	(3, '+ новости'),
	(4, '+ личный кабинет');
/*!40000 ALTER TABLE `st_list_project_type_variants` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
