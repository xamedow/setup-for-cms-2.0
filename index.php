<?php
    namespace main;
    use main\ErrorHandler as e;

# Определяем константы.
    define('RURI', $_SERVER['REQUEST_URI']);
    define('DROOT', $_SERVER['DOCUMENT_ROOT']);
    define('HHOST', $_SERVER['HTTP_HOST']);

# Подключаем автозагрузку классов.
    require_once 'rest/classes/main/Autoloader.php';
    try {
# Определяем обработчик ошибок.
        e::init();
# Читаем конфигурационный файл базы данных.
//        $db = Misc::get_param(FileHandler::readToArray('conf/db'));
//
//# Соединяемся с бд и выставляем кодировку.
//        Db::init($db);
//        Db::queryExec("SET NAMES 'utf8'", array(), false);

        new Router();
    } catch (\Exception $e) {
        e::log('common_errors', $e, true);
        die();
    }